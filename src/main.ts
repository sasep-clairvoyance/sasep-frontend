import Vue from "vue";
import App from "./App.vue";
import keycloak from "./plugins/keycloak";
import router from "./router";
import vuetify from "./plugins/vuetify";
import Notifications from 'vue-notification'
import VueDisqus from 'vue-disqus'
import axios from 'axios'

Vue.use(VueDisqus, {
    shortname: 'sasep'
});

Vue.config.productionTip = false;
Vue.use(Notifications);

export function inform(message: string) {
    Vue.notify({
                   group: 'info',
                   text: message
               })
}

keycloak.init({}).then(() => {

    localStorage.setItem("vue-token", <string>keycloak.token);
    localStorage.setItem("vue-refresh-token", <string>keycloak.refreshToken);

    setAuthorizationHeader();

    setInterval(() => {
        keycloak.updateToken(70)
                .then(refreshed => {
                    if (refreshed) {
                        setAuthorizationHeader();
                    }
                })
                .catch((e) => {
                    console.error('Failed to refresh token');
                    console.error(e);
                });


    }, 60000);

    new Vue({
                router,
                vuetify,
                render: h => h(App)
            }).$mount("#app");

}).catch(() => {
    console.error("Authenticated Failed");
});

function setAuthorizationHeader() {
    if (keycloak.authenticated && keycloak.token !== undefined) {
        axios.defaults.headers["Authorization"] = "Bearer " + keycloak.token;
    } else {
        delete axios.defaults.headers["Authorization"];
    }
}