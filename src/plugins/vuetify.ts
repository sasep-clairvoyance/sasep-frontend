import Vue from "vue";
import Vuetify from "vuetify/lib";

import colors from "vuetify/lib/util/colors"

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: colors.lightGreen.darken3, // #E53935
        secondary: colors.lightGreen.lighten4, // #FFCDD2
        accent: colors.red.darken2, // #3F51B5
      }
    }
  }
});
