import Keycloak from "keycloak-js";

const keycloak = Keycloak({
                            url: process.env.VUE_APP_KEYCLOAK_URL,
                            realm: 'sasep',
                            clientId: 'sasep-frontend'
                        });

export default keycloak;