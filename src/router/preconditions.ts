import keycloak from "@/plugins/keycloak";
import axios from "axios";
import {Route} from "vue-router";
import Vue from "vue";

interface Precondition {
    evaluate(to: Route, from: Route, next: Function): void
}

function showError(message: string) {
    Vue.notify({
                   group: 'error',
                   text: message
               });
}

export default class PreconditionHandler {
    preconditions: Array<Precondition>;
    currentCondition: number;
    to: Route;
    from:Route;
    next: Function;

    constructor(to:Route,from:Route, next:Function, preconditions: Array<Precondition>) {
        this.preconditions = preconditions;
        this.to = to;
        this.from = from;
        this.next = next;
        this.currentCondition = 0;
    }

    evaluate = () => {
        // console.log("evaluating condition " + this.currentCondition);
        if (this.currentCondition < this.preconditions.length - 1) {
            this.preconditions[this.currentCondition].evaluate(this.to, this.from, this.evaluateNext)
        } else {
            this.preconditions[this.currentCondition].evaluate(this.to, this.from, this.next)
        }
    };

    evaluateNext = (alternative?: Route) => {
        if (alternative) {
            this.next(alternative)
        } else {
            this.currentCondition++;
            this.evaluate();
        }
    };
}

export const Preconditions: { [key: string]: Precondition } = {
    requiresAuth: {
        evaluate(to: Route, from: Route, next: Function): void {
            if (keycloak.authenticated) {
                next()
            } else {
               keycloak.login()
            }
        }
    },
    hasContributorRights: {
        evaluate(to: Route, from: Route, next: Function): void {
            if (keycloak.hasRealmRole("contributor")) {
                next()
            } else {
                next({name: "request-contributor-rights"});
                showError("You need contributor rights to access this page.");
            }
        }
    },
    ownsBlog: {
        evaluate(to: Route, from: Route, next: Function): void {
            axios.get(process.env.VUE_APP_SASEP_API_URL + "/user/contribution/news/?simple=true&no-cover=true")
                 .then((result: any) => {
                     if (result.data.some((blog: any) => blog.id === parseInt(to.params.id))) {
                         next();
                     } else {
                         showError("You can't view this blog because its not your own.");
                     }
                 })
                 .catch(error => {
                     showError("Error while checking rights.");
                     console.error(error)
                 })
        }
    }
};
