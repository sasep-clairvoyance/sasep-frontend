import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home.vue"
import Sports from "../views/Sports.vue"
import Teams from "../views/Teams.vue"
import Team from "../views/Team.vue"
import CompareTeams from "../views/CompareTeams.vue"
import User from "../views/User.vue"
import Register from "../views/Register.vue"
import News from "../views/News.vue"
import Blog from "../views/Blog.vue"
import WriteBlog from "../views/WriteBlog.vue"
import Contribution from "../views/Contribution.vue"
import RequestContributorRights from "../views/RequestContributorRights.vue"
import RequestLeagueAccess from "../views/RequestLeagueAccess.vue"

import PreconditionHandler, {Preconditions} from "@/router/preconditions";

Vue.use(VueRouter);


const routes = [
    {
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/about",
        name: "about",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/About.vue")
    },
    {
        path: "/sports",
        name: "sports",
        component: Sports,
    },
    {
        path: "/teams",
        name: "teams",
        component: Teams,
    },
    {
        path: "/team/:id",
        name: "team",
        component: Team,
    },
    {
        path: "/compare",
        name: "compare",
        component: CompareTeams
    },
    {
        path: "/profile",
        name: "profile",
        component: User,
        meta: {
            preconditions: [
                Preconditions.requiresAuth
            ]
        }
    },
    {
        path: "/register",
        name: "register",
        component: Register
    },
    {
        path: "/news",
        name: "news",
        component: News
    },
    {
        path: "/contribute",
        name: "contribute",
        component: Contribution,
        meta: {
            preconditions: [
                Preconditions.requiresAuth,
                Preconditions.hasContributorRights
            ]
        }
    },
    {
        path: "/contribute/news",
        name: "write-blog",
        component: WriteBlog,
        meta: {
            preconditions: [
                Preconditions.requiresAuth,
                Preconditions.hasContributorRights
            ]
        }
    },
    {
        path: "/contribute/news/:id",
        name: "write-blog-id",
        component: WriteBlog,
        meta: {
            preconditions: [
                Preconditions.requiresAuth,
                Preconditions.hasContributorRights,
                Preconditions.ownsBlog
            ]
        }
    },
    {
        path: "/news/:id",
        name: "article",
        component: Blog
    },
    {
        path: "/news/preview/:id",
        name: "preview",
        component: Blog,
        meta: {
            preview: true,
            preconditions: [
                Preconditions.requiresAuth,
                Preconditions.hasContributorRights,
                Preconditions.ownsBlog
            ]
        }
    },
    {
        path: "/rights/contribute",
        name: "request-contributor-rights",
        component: RequestContributorRights,
        meta: {
            preconditions: [
                Preconditions.requiresAuth
            ]
        }
    },
    {
        path: "/rights/access",
        name: "request-league-access",
        component: RequestLeagueAccess,
        meta: {
            preconditions: [
                Preconditions.requiresAuth
            ]
        }
    }
];

const router = new VueRouter({
                                 mode: "history",
                                 base: process.env.BASE_URL,
                                 routes
                             });

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.preconditions)) {
        new PreconditionHandler(to, from, next, to.meta.preconditions).evaluate()
    } else {
        next()
    }
});

export default router;
