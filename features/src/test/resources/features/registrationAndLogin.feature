Feature: Registration and Login
#Business need: verification of user identity of regular users

# We don't test registration as we usr keycloak for user-management and registration is completely done by it.

  Scenario: Successful login
    Given I am "not logged in"
    When I click login
    And I type "test" as "username"
    And I type "test" as "password"
    And I submit "password"
    Then I am signed in

  Scenario: Failed login
    Given I am "not logged in"
    When I click login
    And I type "test" as "username"
    And I type "abcd" as "password"
    And I submit "password"
    Then an error is shown
