Feature: Become a contributor

  Scenario: Open dialog from profile page
    Given I login as "test2" with "test"
    When I click "test2"
    And I click "Contribute"
    Then I am redirected to page "rights/contribute"


  Scenario Outline: Open dialog from other page logged in
    Given I am "logged in"
    And I am on the "<page>" page
    When I click "Contributing now !"
#    To request rights you have to sign in, if a session already exists the user will be automatically logged in.
#    But a redirection to login page is always triggered
    Then I am requested to log in
    Examples:
      | page      |
      | news/1   |

  Scenario Outline: Open dialog from other page not logged in
    Given I am "not logged in"
    And I am on the "<page>" page
    When I click "Contributing now !"
    Then I am requested to log in
    Examples:
      | page      |
      | news/1 |

#    We don not send emails yet

#  Scenario: Request contributor rights
#    Given I am "logged in"
#    And I am on the "request-contributor-rights" page
#    When I enter "soccer" in "contribute-by"
#    And I check "terms-of-service"
#    Then an e-mail is send

#  We have no terms of service at the moment

#  Scenario: Request contributor rights terms of service not accepted
#    Given I am "logged in"
#    And I am on the "request-contributor-rights" page
#    When I enter "soccer" in "contribute-by"
#    Then the error "You need to accept our terms of service" gets displayed
