Feature: View our Data
#Business Need: quick and easy to understand navigation options for casual users

  Scenario Outline: Casual user opens a sport
    Given I am on the "sports" page
    And I am "not logged in"
    When I click "open" on entry "<sport>"
    Then I am redirected to page "teams" with query "sportId=<id>"
    Examples:
      | sport              | id  |
      | League of Legends  | 1   |

  Scenario Outline: Casual user opens a league
    Given I am on the "sports" page
    And I am "not logged in"
    When I click "anywhere" on entry "<sport>"
    And I click "anywhere" on entry "<league>"
    Then I am redirected to page "teams" with query "league=<id>"
    Examples:
      | sport              | league                       | id  |
      | League of Legends  | League Championship Series   | 10  |
      | League of Legends  | League Championship Series 2 | 11  |

  Scenario Outline:  User opens a team
    Given I am on the "teams?sportId=<sportId>" page
    When I click "anywhere" on entry "<team>"
    Then I am redirected to page "team/<teamId>"
    Examples:
      | sportId    | team            | teamId |
      | 1          | Cloud9          | 100    |
      | 1          | Evil Geniuses   | 101    |
      | 1          | 100 Thieves     | 102    |
      | 1          | FlyQuest        | 103    |

#    Current matchups aren't implemented yet

#  Scenario Outline: User clicks a matchup label
#    Given I start on the <matchups> page
#    When I click the <thismatchup> button
#    Then I land on the <matchup> page
#    Examples:
#      | matchups               | thismatchup                   | matchup           |
#      | /football/fcbarcelona/ | FC Barcelona vs FC Chelsea    | /matchup12765432/ |
#      | /football/fcbarcelona/ | FC Barcelona vs FC Bayern     | /matchup32123409/ |
#      | /lol/invictusgaming/   | Invictus Gaming vs G2 Esports | /matchup00321321/ |


#  Search function isn't implemented yet

#  Scenario: User opens search feature
#    Given I am on the "start" page
#    When I click the "search" symbol
#    Then the "search textfield" opens
#
#  Scenario Outline: User enters search term and presses enter
#    Given I am on the "start" page
#    And the "search textfield" opens
#    When the I type the <term>
#    And I press "enter"
#    Then I land on the <page> page
#    Examples:
#      | term            | page                   |
#      | no hit          | /                      |
#      | football        | /football/             |
#      | LeagueOfLegends | /lol/                  |
#      | FC Barcelona    | /football/fcbarcelona/ |
#      | Invictus Gaming | /lol/invictusgaming/   |

