Feature: Search and view a blog

  Scenario Outline: Open a blog
    Given I am on the "news" page
    When I click "anywhere" on entry "<articleCover>"
    Then I am redirected to page "<article>"
    Examples:
      | articleCover         | article |
      | Some cool CSGO event | news/1  |
#      | article1              | /blog/123    |


  Scenario Outline: Filter blogs by sport
    Given I am on the "news" page
    When I select "<sport>" as "Sport"
#    No data available at the moment
#    Then I only see articles tagged with <sport>
    Examples:
      | sport             |
      | League of Legends |
#      | LeagueOfLegends |
#      | basketball      |
#      | starcraft2      |


  Scenario Outline: Filter blogs by team
    Given I am on the "news" page
    When I select "<team>" as "Team"
#    No data available at the moment
#    Then I only see articles tagged with <team>
    Examples:
      | team          |
      | Cloud9        |
      | Evil Geniuses |
      | 100 Thieves   |
      | FlyQuest      |

  Scenario Outline: Sort blogs
    Given I am on the "news" page
    When I select "<order>" as "order by"
#    No data available at the moment
#    Then Blogs are sorted by <order>
    Examples:
      | order    |
      | Latest   |
      | Alphabet |
      | Best   |
