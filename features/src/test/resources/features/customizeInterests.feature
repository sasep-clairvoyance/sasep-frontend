Feature: Customize Interests

  Scenario: Add interest for a sport
    Given I am "logged in"
    When I open the "sports" page
    And I click "like" on entry "League of Legends"
    Then I like "League of Legends"
    And the message "You are now interested in League of Legends" shows up

  Scenario: Add interest for a sport without account
    Given I am "not logged in"
    When I open the "sports" page
    Then button "like" is not visible for entry "League of Legends"

  Scenario: Remove interest for a sport
    Given I am "logged in"
    And I am interested in sport "League of Legends"
    When I open the "sports" page
    And I click "like" on entry "League of Legends"
    Then I don't like "League of Legends"
    And the message "You are no longer interested in League of Legends" shows up


  Scenario: Add interest for a team from list
    Given I am "logged in"
    When I open the "teams?sportId=1" page
    And I click "like" on entry "Cloud9"
    Then I like "Cloud9"
    And the message "You are now interested in Cloud9" shows up

  Scenario: Add interest for a team from list without account
    Given I am "not logged in"
    When I open the "teams?sportId=1" page
    Then button "like" is not visible for entry "Cloud9"

  Scenario: Remove interest for a team from list
    Given I am "logged in"
    And I am interested in team "Cloud9"
    When I open the "teams?sportId=1" page
    And I click "like" on entry "Cloud9"
    Then I don't like "Cloud9"
    And the message "You are no longer interested in Cloud9" shows up


  Scenario: Add interest for a team
    Given I am "logged in"
    When I open the "team/100" page
    And I click "like" on entry "Cloud9"
    Then I like "Cloud9"
    Then the message "You are now interested in Cloud9" shows up

  Scenario: Add interest for a team without account
    Given I am "not logged in"
    When I open the "team/100" page
    Then button "like" is not visible for entry "Cloud9"

  Scenario: Remove interest for a team
    Given I am "logged in"
    And I am interested in team "Cloud9"
    When I open the "team/100" page
    And I click "like" on entry "Cloud9"
    Then I don't like "Cloud9"
    And the message "You are no longer interested in Cloud9" shows up
