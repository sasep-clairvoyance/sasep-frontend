Feature: Compare two teams

  Scenario: Compare A vs B and A wins
    Given I am on the "compare" page
    When I select team "Cloud9" as "first-team"
    And I select team "Evil Geniuses" as "second-team"
    Then "Cloud9" is displayed
    And "Evil Geniuses" is displayed
    And "first-team" would win with "70%" chance
    And "second-team" would win with "30%" chance

  Scenario: Compare A vs B and B wins
    Given I am on the "compare" page
    When I select team "Evil Geniuses" as "first-team"
    And I select team "Cloud9" as "second-team"
    Then "Cloud9" is displayed
    And "Evil Geniuses" is displayed
    And "first-team" would win with "30%" chance
    And "second-team" would win with "70%" chance

  Scenario: Compare A vs C and drawn
    Given I am on the "compare" page
    When I select team "Cloud9" as "first-team"
    And I select team "100 Thieves" as "second-team"
    Then "Cloud9" is displayed
    And "100 Thieves" is displayed
    And "first-team" would win with "50%" chance
    And "second-team" would win with "50%" chance

  Scenario: Compare A vs A fails
    Given I am on the "compare" page
    When I select team "Cloud9" as "first-team"
    And I select team "Cloud9" as "second-team"
    Then "Cloud9" is displayed
    And "first-team" would win with "100%" chance
    And "second-team" would win with "0%" chance