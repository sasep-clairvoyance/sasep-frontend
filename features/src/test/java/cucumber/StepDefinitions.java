package cucumber;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class StepDefinitions {

    private WebDriver driver;

    private boolean loggedIn;

    private static final String BASE_URL = "http://localhost:8080/";
    private static final Map<String, String> BUTTONS = new HashMap<>();

    static {
        BUTTONS.put("open", "mdi-open-in-new");
        BUTTONS.put("like", "mdi-star");
    }

    @Before
    public void setup() {
        System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
        driver = new FirefoxDriver();
        driver.get(BASE_URL);
    }

    @After
    public void end() {
        driver.close();
    }


    @Given("I am on the {string} page")
    public void i_am_on_page(String page) {
        driver.get(BASE_URL + page);
    }

    @When("I open the {string} page")
    public void i_open_page(String page) {
        driver.get(BASE_URL + page);
        if (loggedIn) {
            driver.findElement(By.className("mdi-login")).click();
            login();
        }
    }

    @Given("I am {string}")
    public void i_am(String state) {
        final List<WebElement> logoutButton = driver.findElements(By.className("mdi-logout"));
        final List<WebElement> loginButton = driver.findElements(By.className("mdi-login"));

        if (state.equals("not logged in")) {
            loggedIn = false;
            if (loginButton.isEmpty()) {
                logoutButton.get(0).click();
            }
            return;
        } else if (state.equals("logged in")) {
            loggedIn = true;
            if (logoutButton.isEmpty()) {
                loginButton.get(0).click();
                login();
            }
            return;
        }
        fail("Error while checking login state");
    }

    @Given("I login as {string} with {string}")
    public void login_as(String name, String password) {
        driver.findElement(By.className("mdi-login")).click();
        login(name, password);
    }

    @Given("^I am interested in (sport|team) \"(.*)\"$")
    public void set_interest(String type, String entry) {
        if (type.equals("sport")) {
            return;
        } else if (type.equals("team")) {
            return;
        }
        fail();
    }

    @When("I click {string} on entry {string}")
    public void click_button_for_entry(String button, String text) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.xpath("//*[contains(text(),'" + text + "')]/../..")).isEmpty());
        WebElement element = driver.findElement(By.xpath("//*[contains(text(),'" + text + "')]/../.."));
        if (button.equals("anywhere")) {
            element.click();
        } else {
            element.findElement(By.xpath(".//*[contains(concat(' ', normalize-space(@class), ' '), ' " + BUTTONS.get(button) + " ')]")).click();
        }
    }

    @When("I select team {string} as {string}")
    public void select_team(String teamName, String teamPosition) {
        if (teamPosition.equals("first-team")) {
            driver.findElement(By.id("change-team-1")).click();
            click_button_for_entry("anywhere", teamName);
            return;
        } else if (teamPosition.equals("second-team")) {
            driver.findElement(By.id("change-team-2")).click();
            click_button_for_entry("anywhere", teamName);
            return;
        }
        fail("Error while selecting team " + teamName);
    }

    @When("I select {string} as {string}")
    public void use_select(String value, String label) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.xpath("//label[contains(text(),'" + label + "')]")).isEmpty());
        driver.findElement(By.xpath("//label[contains(text(),'" + label + "')]/../..")).click();
        driver.findElement(By.className("v-list"))
              .findElement(By.xpath(".//*[contains(text(),'" + value + "')]")).click();
    }

    @When("I click {string}")
    public void i_click(String value) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.xpath("//*[contains(text(),'" + value + "')]")).isEmpty());
        driver.findElement(By.xpath(".//*[contains(text(),'" + value + "')]")).click();
    }

    @When("I click login")
    public void i_click_login() {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.className("mdi-login")).isEmpty());
        driver.findElement(By.className("mdi-login")).click();
    }

    @When("I type {string} as {string}")
    public void i_type_in(String value, String id) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.id(id)).isEmpty());
        driver.findElement(By.id(id)).sendKeys(value);
    }

    @When("I submit {string}")
    public void i_submit(String id) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.id(id)).isEmpty());
        driver.findElement(By.id(id)).submit();
    }

    @Then("{string} is displayed")
    public void is_displayed(String text) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.xpath("//*[contains(text(),'" + text + "')]")).isEmpty());
    }

    @Then("button {string} is not visible for entry {string}")
    public void button_hidden_for_entry(String button, String text) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.xpath("//*[contains(text(),'" + text + "')]/../..")).isEmpty());
        WebElement element = driver.findElement(By.xpath("//*[contains(text(),'" + text + "')]/../.."));
        List<WebElement> buttons = element.findElements(By.xpath(".//*[contains(concat(' ', normalize-space(@class), ' '), ' " + BUTTONS.get(button) + " ')]"));
        assertTrue(buttons.isEmpty());
    }

    @Then("I am redirected to page {string} with query {string}")
    public void redirected_to_with_query(String page, String query) throws MalformedURLException {
        URL url = new URL(driver.getCurrentUrl());
        assertEquals("/" + page, url.getPath());
        assertEquals(query, url.getQuery());
    }

    @Then("I am redirected to page {string}")
    public void redirected_to(String page) throws MalformedURLException {
        URL url = new URL(driver.getCurrentUrl());
        assertEquals("/" + page, url.getPath());
    }

    @Then("{string} would win with {string} chance")
    public void check_win_chance(String teamPosition, String winChance) {
        WebElement element = null;
        if (teamPosition.equals("first-team")) {
            element = driver.findElement(By.id("team-1-chance"));
        } else if (teamPosition.equals("second-team")) {
            element = driver.findElement(By.id("team-2-chance"));
        }

        if (element == null) {
            fail("Error while checking win chance for team " + teamPosition);
            return;
        }

        assertEquals(winChance, element.getText());
        assertEquals("width: " + winChance + ";", element.findElement(By.xpath(".//../..")).getAttribute("style"));
    }

    @Then("^I (like|don't like) \"(.*)\"$")
    public void i_like(String like, String entry) {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.xpath("//*[contains(text(),'" + entry + "')]/../..")).isEmpty());
        WebElement element = driver.findElement(By.xpath("//*[contains(text(),'" + entry + "')]/../.."));
        WebElement likeButton = element.findElement(By.xpath(".//*[contains(concat(' ', normalize-space(@class), ' '), ' mdi-star ')]"));

        if (like.equals("like")) {
            assertEquals("rgb(85, 139, 47)", likeButton.getCssValue("caret-color"));
            return;
        } else if (like.equals("don't like")) {
            assertEquals("rgb(224, 224, 224)", likeButton.getCssValue("caret-color"));
            return;
        }
        fail();
    }

    @Then("the message {string} shows up")
    public void message_shows_up(String message) {
        WebElement infoMessages = driver.findElement(By.id("info-messages"));
        (new WebDriverWait(driver, 10)).until(d -> !infoMessages.findElements(By.xpath(".//*[contains(text(),'" + message + "')]")).isEmpty());
    }

    @Then("I am requested to log in")
    public void request_login() {
        (new WebDriverWait(driver, 10)).until((ExpectedCondition<Boolean>) d -> d.getTitle().toLowerCase().startsWith("log in"));
    }

    @Then("I am signed in")
    public void check_signed_in() {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.className("mdi-logout")).isEmpty());
    }

    @Then("an error is shown")
    public void check_for_errors() {
        (new WebDriverWait(driver, 10)).until(d -> !d.findElements(By.className("alert")).isEmpty());
    }

    private void login() {
        login("test", "test");
    }

    private void login(String name, String password) {
        (new WebDriverWait(driver, 10)).until((ExpectedCondition<Boolean>) d -> d.getTitle().toLowerCase().startsWith("log in"));

        driver.findElement(By.name("username")).sendKeys(name);
        driver.findElement(By.name("password")).sendKeys(password);

        driver.findElement(By.name("password")).submit();
    }
}
